var express = require('express');
var m_user = require('../models/user');
var router = express.Router();


router.get('/', function(req, res) {
  res.render('login');
});



router.post('/', function(req, res) {
	if(!req.body){
		res.send('No Data! <a href="/login">Retry</a>\n');
		return false;
	}

	m_user.find({ email: req.body.email, password: req.body.password }, function(err, docs){
		if(err) return console.error(err);

		if(docs.length==0){
			res.send('No User.. <a href="/login">Retry</a>\n');
		}else{
			res.send('Welcome '+docs[0].username);
		}
	});
		
});


module.exports = router;
