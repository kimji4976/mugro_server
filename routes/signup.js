var express = require('express');
var m_user = require('../models/user');
var router = express.Router();


router.get('/', function(req, res) {
	res.render('signup');
});



router.post('/', function(req, res) {
	if(!req.body){
		res.send('No Data! <a href="/signup">Retry</a>\n');
		return false;
	}

	var user = new m_user({
		username: req.body.username,
		email: req.body.email,
		password: req.body.password
	});


	user.save(function(err){
		if(err) return console.error(err);

		console.log('User Saved Successfully');

		res.send('Sign up successfully!! <a href="/">Go to index page.</a>');
	});
});


module.exports = router;
